import React from 'react'
import Insertrate from './Insertrate'
import { Route,BrowserRouter,Routes } from 'react-router-dom'
import { Card,CardImg, CardText, CardTitle,Table } from 'react-bootstrap'
import {useEffect,useState} from 'react'
import axios from 'axios'



function Loans() {
  const [data,setData]=useState([])
  useEffect(()=>{
    const fecthData=async()=>{
    try{
      let response=await axios.get("http://localhost:3001/fetch")
      console.log(response.data)
      setData(response.data)
    }catch (err){
      throw err
    }
  }
  fecthData()
  },[])

  return (
    < >

    <div class='cointaner mx-5 text-center' style={{marginTop:"50px"}}>
      
    <Card className="col-lg-4 col-md-4 col-sm-12" style={{width:'90%',marginRight:'10PX', textAlign:"center"}} >
        <CardImg  src=" https://toplist.vn/images/800px/moneyview-972581.jpg"  alt="Card image cap" />
        <CardText style={{color:"blue"}}><h1>Personal Loan Interest Rates 2023</h1></CardText>
        <CardTitle style={{color:"Highlight"}}>Our Interest Rates starting at just 1.33% per month</CardTitle>
        </Card>
    </div>


 {/* <div class='cointaner mx-5 text-center'> 
 <div className="row col-lg gap-5 mx-5 " >
    <Card className="col-lg-4 col-md-4 col-sm-12"style={{ width:'15rem', marginTop:"20px"}}>
    <CardTitle>
    Top Features and Benefits of Personal Loan
 </CardTitle>

      <Card.Img variant="top" src="https://moneyview.in/images/Asset%208.png"  width={"50px"}/>
      <Card.Body>
        <Card.Title style={{color:"springgreen"}}>Flexible Loan Amount</Card.Title>
        <Card.Text>
        Choose any loan amount starting from Rs. 5,000 upto Rs. 10 lakhs.
        </Card.Text>
      </Card.Body>
    </Card>


    <Card className="col-lg-4 col-md-4 col-sm-12"style={{ width: '15rem', marginTop:"20px"}}>
      <Card.Img variant="top" src="https://moneyview.in/images/Asset%209.png" />
      <Card.Body>
        <Card.Title style={{color:"springgreen"}}>Quick Eligibility Check</Card.Title>
        <Card.Text>
        Your eligibility check from moneyview website or download the app takes only 2 minutes saving energy and time.
        </Card.Text>
      </Card.Body>
    </Card>

    <Card className="col-lg-4 col-md-4 col-sm-12"style={{ width: '15rem', marginTop:"20px"}}>
      <Card.Img variant="top" src="https://moneyview.in/images/Asset%2010.png" />
      <Card.Body>
        <Card.Title style={{color:"springgreen"}}>Collateral Free Loans</Card.Title>
        <Card.Text>
        To apply for a personal loan through moneyview, you don’t have to pledge any asset or arrange for a guarantor
        </Card.Text>
      </Card.Body>
    </Card>


    <Card className="col-lg-4 col-md-4 col-sm-12"style={{ width: '15rem', marginTop:"20px"}}>
      <Card.Img variant="top" src="https://moneyview.in/images/Asset%2011.png" />
      <Card.Body>
        <Card.Title style={{color:"springgreen"}}>Affordable Interest Rates</Card.Title>
        <Card.Text>
        We understand how important interest rates are as they determine the EMI amount to be paid. At moneyview the interest rate starts at just 1.33% per month
        </Card.Text>
      </Card.Body>
    </Card>

    <h5 style={{textAlign:"center"}}>Personal Loan Interest Rates and Charges</h5>
Interest rates and other charges for personal loans at moneyview are transparent and affordable with no hidden costs. Take a look at the applicable charges below.
    </div>  */}

<div class="container mx-5 " >
<div class='row gap-5 mx-5' >
   {data.filter((e)=>(e.Img4,e.Tittle4,e.Text4)).map((e)=>(
    <div className="col-lg-4 col-md-4 col-sm-12 card border-0 mt-3" key={e.Img4}>
          <img src={e.Img4} alt="" className='w-60 rounded-3'/>
          <div className='card-body text-center'>
          <p className='text-dark '>{e.Tittle4}</p>
            <p className='text-dark fs-5'>{e.Text4}</p>
            
          </div>
    </div>
  ))}
</div>
</div>



<div class='cointaner mx-5 text-center'> 
 <div className="row col-lg gap-5 mx-5 " >
 <CardTitle>
 <h5 style={{textAlign:"center",color:'blue'}}>
  Personal Loan Interest Rates and Charges</h5>
Interest rates and other charges for personal loans at moneyview are transparent and affordable with no hidden costs. Take a look at the applicable charges below.
</CardTitle>
 
    <Table striped bordered hover>
      <thead>
        <tr>
          <th style={{color:'seagreen'}} >Fees and Charges</th>
          <th style={{ color:'seagreen'}}>Amount Chargeable</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Interest Rate</td>
          <td>Starting from 1.33% per month</td>
          
        </tr>
        <tr>
          <td>Loan Processing Charges</td>
          <td>Starts at 2% of the approved loan amount</td>
          
        </tr>
        <tr>
          <td>Interest on Overdue EMIs</td>
          <td colSpan={2}>	2% per month on the overdue EMI/Principal loan amount</td>
         
        </tr>
        <tr>
          <td>Cheque Bounce</td>
          <td colSpan={2}>	Rs.500/- each time</td>
          
        </tr>
        <tr>
          <td>Loan Cancellation</td>
          <td colSpan={2}>If a borrower wishes to exit the loan after it has been sanctioned, he/she can do so within 3 days of loan disbursement.<br/>
The principal amount and other applicable charges will have to be paid.</td>
          
        </tr>
      </tbody>
    </Table>

    
    <Card style={{backgroundColor:'cadetblue'}}> 
    <h4 style={{textAlign:'center',backgroundColor:'hotpink'}}>RBI Repo Rate 2023 - Recent News</h4>
      <CardText>
      Repo rate has been hiked by  <u><h7 style={{color:'green'}}> basis points to 6.25% by</h7></u> the RBI’s Monetary Policy Committee (MPC). This step has been taken to manage inflation and will now cost more for banks to borrow funds from the Central Bank. This in turn can lead to a hike in interest rates and overall EMIs. But this increase will be beneficial for FD holders.
      </CardText>
    </Card>
    </div>
    </div>

    </>
  )
}

export default Loans