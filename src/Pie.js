// import React from 'react'
// const loanAmountInput = document.querySelector(".loan-amount");
// const interestRateInput = document.querySelector(".interest-rate");
// const loanTenureInput = document.querySelector(".loan-tenure");

// const loanEMIValue = document.querySelector(".loan-emi .value");
// const totalInterestValue = document.querySelector(".total-interest .value");
// const totalAmountValue = document.querySelector(".total-amount .value");

// const calculateBtn = document.querySelector(".calculate-btn");

// let loanAmount = parseFloat(loanAmountInput.value);
// let interestRate = parseFloat(interestRateInput.value);
// let loanTenure = parseFloat(loanTenureInput.value);

// let interest = interestRate / 12 / 100;

// let myChart;
// let loanAmountValue = loanAmountInput.value;
//   let interestRateValue = interestRateInput.value;
//   let loanTenureValue = loanTenureInput.value;



// function Pie() {
//   return (
//     <div>


  
// const checkValues = () => {
//   let regexNumber = /^[0-9]+$/;
//   if (!loanAmountValue.match(regexNumber)) {
//     loanAmountInput.value = "10000";
//   }

//   if (!loanTenureValue.match(regexNumber)) {
//     loanTenureInput.value = "12";
//   }

//   let regexDecimalNumber = /^(\d*\.)?\d+$/;
//   if (!interestRateValue.match(regexDecimalNumber)) {
//     interestRateInput.value = "7.5";
//   }
// };

// const displayChart = (totalInterestPayableValue) => {
//   const ctx = document.getElementById("myChart").getContext("2d");
//   myChart = new Chart(ctx, {
//     type: "pie",
//     data: {
//       labels: ["Total Interest", "Principal Loan Amount"],
//       datasets: [
//         {
//           data: [totalInterestPayableValue, loanAmount],
//           backgroundColor: ["#e63946", "#14213d"],
//           borderWidth: 0,
//         },
//       ],
//     },
//   });
// };

// const updateChart = (totalInterestPayableValue) => {
//   myChart.data.datasets[0].data[0] = totalInterestPayableValue;
//   myChart.data.datasets[0].data[1] = loanAmount;
//   myChart.update();
// };

// const refreshInputValues = () => {
//   loanAmount = parseFloat(loanAmountInput.value);
//   interestRate = parseFloat(interestRateInput.value);
//   loanTenure = parseFloat(loanTenureInput.value);
//   interest = interestRate / 12 / 100;
// };

// const calculateEMI = () => {
//   checkValues();
//   refreshInputValues();
//   let emi =
//     loanAmount *
//     interest *
//     (Math.pow(1 + interest, loanTenure) /
//       (Math.pow(1 + interest, loanTenure) - 1));

//   return emi;
// };

// const updateData = (emi) => {
//   loanEMIValue.innerHTML = Math.round(emi);

//   let totalAmount = Math.round(loanTenure * emi);
//   totalAmountValue.innerHTML = totalAmount;

//   let totalInterestPayable = Math.round(totalAmount - loanAmount);
//   totalInterestValue.innerHTML = totalInterestPayable;

//   if (myChart) {
//     updateChart(totalInterestPayable);
//   } else {
//     displayChart(totalInterestPayable);
//   }
// };

// const init = () => {
//   let emi = calculateEMI();
//   updateData(emi);
// };

// init();

// calculateBtn.addEventListener("click", init); 


//     </div>
//   )
// }

// export default Pie



// import React from 'react';
// export const data = {
//   labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
//   datasets: [
//     {
//       label: '# of Votes',
//       data: [12, 19, 3, 5, 2, 3],
//       backgroundColor: [
//         'rgba(255, 99, 132, 0.2)',
//         'rgba(54, 162, 235, 0.2)',
//         'rgba(255, 206, 86, 0.2)',
//         'rgba(75, 192, 192, 0.2)',
//         'rgba(153, 102, 255, 0.2)',
//         'rgba(255, 159, 64, 0.2)',
//       ],
//       borderColor: [
//         'rgba(255, 99, 132, 1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)',
//         'rgba(75, 192, 192, 1)',
//         'rgba(153, 102, 255, 1)',
//         'rgba(255, 159, 64, 1)',
//       ],
//       borderWidth: 1,
//     },
//   ],
// };

// export function Pie() {
//   return <Pie data={data} />;
// }
// export default Pie

 
/// Simple Calculator

// import React,{useState, useEffect} from 'react';
// // import './App.css';

// function Pie() {
//   const [currentSum,setCurrentSum]=useState(0);
//   const [clear,setClear]=useState(false);

//   useEffect(()=>{
//     document.querySelector('#result').value="";
//   },[])
  
//   useEffect(()=>{
//     if(clear)
//     document.querySelector('#result').value="";
//   })

//   const Add=(e)=>{
//     e.preventDefault();
//     if(clear) setClear(false);
//     let currentNum=document.querySelector('#num').value
//     if(currentNum=='')
//     return;
//     let sum= currentSum+parseInt(currentNum);
//     setCurrentSum(sum);
//     document.querySelector('#num').value="";
      
//   }

//   const Clear=(e)=>{
//     e.preventDefault();
//     console.log('sum:', currentSum);
//     document.querySelector('form').reset();
//     setClear(true);
//     setCurrentSum(0);
//   }

//   return (
//     <div className="App" style={{marginTop:'50px'}}>
//       <div className="app-title">
//         <h1> Basic Form Calculator</h1>
//       </div>
//       <form>
//             <input type="text" id="result" value={currentSum}  readOnly />   
//             <input type="text" id="num" placeholder="enter a number" />
//             <button onClick={Add}>Add</button>
//             <button onClick={Clear}>Clear</button>
//       </form>
//     </div>
//   );
// }

// export default Pie;



import React from 'react'
import { Card,Button, CardImg, CardBody, CardText ,Table} from 'react-bootstrap';
import {useEffect,useState} from 'react'
import axios from 'axios';

function Pie() {

// const [data,setData]=useState([])
// useEffect(()=>{
//   const fecthData=async()=>{
//   try{
//     let response=await axios.get("http://localhost:3001/fetch")
//     console.log(response.data)
//     setData(response.data)
//   }catch (err){
//     throw err
//   }
// }
// fecthData()
// },[])

  return (
    <div  class= "cointaner  text-center mt-5 ">
        <div class='row col-lg gap-5 mx-5' >
        <h1> Our Partner</h1>
        <h4>View our list of partners below</h4>

         <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/TCFSL.jpeg"  />
      <Card.Body>
        <Card.Title>Tata Capital Financial Services Limited</Card.Title>
        <Card.Text style={{color:'darkgreen'}}>
        <ul>
            <li> Fully Digital process</li>
            <li>
            Interest Rate starting at 10.99% p.a</li>
            <li> Loan amount from Rs 75,000 to 35 lakh</li>
            <li>Tenure upto 6 years</li>
        </ul>
        </Card.Text>
      </Card.Body>
    </Card>


    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/Fibe.png"  />
      <Card.Body>
        <Card.Title>Fibe</Card.Title>
        <Card.Text >
        <ul>
            <li>  Personal Loan upto ₹5 lakh in just 10 minutes</li>
            <li>
            Instant cash loan</li>
            <li>Interest Rate starting at 10.99% p.a.</li>
            <li> Loan amount from Rs 5,000 to 5 lakh</li>
            <li>Tenure upto 24 months</li>
        </ul>
        </Card.Text>
      </Card.Body>
    </Card>


    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/kreditbee.png"  />
      <Card.Body>
        <Card.Title>Kreditbee (Finovation Tech Solutions Private Limited)</Card.Title>
        <Card.Text style={{color:'darkgreen'}}>
        <ul>
            <li> Loans from ₹ 1,000 to ₹ 3 Lakh</li>
            <li>
            100% online process</li>
            <li> Loan amount from Rs 75,000 to 35 lakh</li>
            <li> Direct bank transfer</li>
        </ul>
        </Card.Text>
      </Card.Body>
    </Card>


    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/chqbook.png"  />
      <Card.Body>
        <Card.Title>Home First Finance Company</Card.Title>
        <Card.Text style={{color:'red'}}>
        <ul>
            <li> - Home Loan & Loan against property for salaried & self employed
</li>
            <li>
            Upto 90% of property value</li>
            <li>  Loans upto 25 years tenure</li>
            <li> Interest Rate starting at 8% p.a. to 18% p.a.</li>
        </ul>
        </Card.Text>
      </Card.Body>
    </Card>

    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/TCHFL.jpg"  />
      <Card.Body>
        <Card.Title>Tata Capital Housing Finance Limited</Card.Title>
        <Card.Text style={{color:'darkgreen'}}>
        <ul>Home Loans
            <li> Home Loan upto Rs 75 Lakh
</li>
            <li>
            Interest Rate starting at 8.60% p.a.</li>
            </ul>
            <ul style={{color:'blueviolet'}}>Loan against Property
            <li>   Interest Rate: starting at 10.10% p.a. for residential & 10.25% p.a. for commercial property</li>
            
        </ul>
        </Card.Text>
      </Card.Body>
    </Card>

    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px"}}>
      <Card.Img variant="top" src="https://www.tatacapitalmoneyfy.com/content/dam/tata-capital-moneyfy/our-partners/homefirst.png"  />
      <Card.Body>
        <Card.Title>Home First Finance Company</Card.Title>
        <Card.Text style={{color:'darkgreen'}}>
        <ul>
            <li>Home Loan & Loan against property for salaried & self employed
</li>
            <li>
            Upto 90% of property value</li>
            <li>  Loans upto 25 years tenure</li>
            <li> Interest Rate starting at 8% p.a. to 18% p.a.</li>
        </ul>
        </Card.Text>
      </Card.Body>
    </Card> 

    </div>
    </div>


// <div class="container mx-5  mt-5" >
// <div class='row gap-5 mx-5' >
//   {data.filter((e)=>(e.Img6,e.Title6,e.Text6)).map((e)=>(
//     <div className="col-lg-4 col-md-4 col-sm-12 card border-0 mt-3" key={e.Img6}>
//           <img src={e.Img} alt="" className='w-100 rounded-3'/>
//           <div className='card-body text-center'>
//             <p className='text-dark fs-5'>{e.Title6}</p>
//             <p className='text-dark '>{e.Text6}</p>
//           </div>
//     </div>
//   ))}
// </div>
// </div> */}


  )
}

export default Pie



