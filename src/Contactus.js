import React from 'react'
import { CardText } from 'react-bootstrap';
import { CardImg,CardTitle,CardBody,Card,CardSubtitle} from 'react-bootstrap'
import { Form, FormGroup,Label,Input} from 'reactstrap';
import {useEffect,useState} from 'react'
import axios from 'axios';

function Contactus() {
  const [data,setData]=useState([])
  useEffect(()=>{
    const fecthData=async()=>{
    try{
      let response=await axios.get("http://localhost:3001/fetch")
      console.log(response.data)
      setData(response.data)
    }catch (err){
      throw err
    }
  }
  fecthData()
  },[])

  return (
    <>
     <div  class= 'cointaner mx-5 text-center mt-5' >
      <div className="row col-lg gap-5 mx-5 ">
        <Card style={{marginTop:'40px'}}>
          <CardText>
         <h1> Get in touch with us </h1> </CardText>
          <CardTitle>  Please write to us at  <u style={{color:"blue"}}>care@moneyview</u>.in for any service-related queries or feedback. Alternatively, you may also use the moneyview App to lodge a complaint or report an issue. We generally respond within 24-48 working hours.
        <h2>
        Timings: Monday to Saturday - 9 am to 6 pm - Excluding public holidays
        </h2></CardTitle>
        </Card>
        </div>
        </div>

    {/* <div class="class='cointaner mx-5 text-center'">
    <div className="row col-lg gap-5 mx-5 ">
    
    
      <Card  className="col-lg-4 col-md-4 col-sm-12 mt-5" >
        <CardImg src="https://moneyview.in/images/customer-care-no.png" >

        </CardImg>
        <CardText>  <h5>Contact Number </h5>
        080 6939 0476</CardText>
      </Card>
      <Card  className="col-lg-4 col-md-4 col-sm-12 mt-5">
        <CardImg src=" https://moneyview.in/images/blog/wp-content/uploads/2019/03/ic-dsa-inquiry-40%402x.png"></CardImg>
          <CardText> care@moneyview.in </CardText>
      </Card>
      <Card>
        <CardImg  src=" https://moneyview.in/images/Contact-us-mv.png " width={'50px'}>

        </CardImg>

      </Card>


    </div>
     </div> */}
    
     <div class="container mx-5 " >
<div class='row gap-5 mx-5' >
   {data.filter((e)=>(e.Img2,e.Text2)).map((e)=>(
    <div className="col-lg-4 col-md-4 col-sm-12 card border-0 mt-3" key={e.Img2}>
          <img src={e.Img2} alt="" className='w-50 rounded-3' />
          <div className='card-body text-center'>
            
             <p className='text-dark '> {e.Text2} </p> 
          </div>
    </div>
  ))}
</div>



<div class='row gap-5 mx-5' >
   {data.filter((e)=>(e.Img3)).map((e)=>(
    <div className="col-lg-4 col-md-4 col-sm-12 card border-0 mt-3" key={e.Img3}>
          <img src={e.Img3} alt="" className=' rounded-3' style={{width:'1000px'}} />
          <div className='card-body text-center'>
          
          
          </div>
    </div>
  ))}
</div>

</div>





    </>
  )
}

export default Contactus;