

import axios from 'axios';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';


import { Form, FormGroup,Label,Input} from 'reactstrap';
import Register from './Register';

function Login(args) {
  const [show, setShow] = useState(false);
  const [formdata,setFormdata] = useState({
    email:'',
    password:''
  })

  const handleClose = (event) =>{
  const {name,value} = event.target
  setFormdata({
    ...formdata,
    [name]:value
  })
}
  console.log(formdata)


  const toggle = (event) => 
  {
    event.preventDefault();
    setShow(!show)
  }
const handleSubmit = async(event) =>{
  event.preventDefault();
  try{
    let response = await axios.get (`http://localhost:3001/login/${formdata.email}/${formdata.password}`)
    console.log(response)
    if (response.data!='')
    {
      alert('Login successful')
    }
else{
  alert("Login successful ")
}
   
    setShow(!show)
  }
  catch (err){
    throw err
  }
}
  return (
    <>
      <Button variant="success" onClick={toggle}>
       Login
      </Button>


      <Modal
        show={show}
        toggle={toggle}
        {...args}
        onHide={handleClose}
        
      >
        <Modal.Header toggle={toggle}>Login
         
        </Modal.Header>
        <Modal.Body>
        <Form onSubmit={handleSubmit}>
        <FormGroup className="">
          <Label for="exampleEmail" className="">Email</Label>
          <Input type="email" name="email" id="exampleEmail" placeholder="something@idk.cool" value={formdata.email} onChange={handleClose}
          required />
        </FormGroup>
        <FormGroup >
          <Label for="examplePassword" className="">Password</Label>
          <Input type="password" name="password" id="Password" placeholder="PaSSword@123" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value={formdata.password}
          onChange={handleClose} 
          required
          />
        </FormGroup>
        <Button type="submit">Submit</Button>
      </Form>
     
  
        </Modal.Body>
        <Modal.Footer>
        
          <Button variant="secondary" onClick={toggle}>
            Close
          </Button>
         <p style={{marginBottom:'80px'}}> <Register /></p>
          
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Login;
