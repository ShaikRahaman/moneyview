// import React from 'react'
// import React,{ useState } from 'react'
// import { Modal } from 'react-bootstrap'
// function Piechart({ piza }) {

//   const [interest, setinterest] = useState(1)
//   const [variant, setvariant] = useState('year')
//   const [show, setShow] = useState(false);

//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);

//   return (

//     <div style={{ margin: '50px' }} className='shadow-lg p-3 mb-100px bg-white rounded'>
//       <div onClick={handleShow}>
//         <h1>
//           {piza.name}
//           <img src={piza.image} className='img-fluid' style={{ height: '200px', width: '200px' }} />
//         </h1>
//       </div>
//       <div className='flex-container'>
//         <div className='w-100 m-1'>
//           <p>varients</p>
//           <select className='form-control' value={variant} onChange={(e) => { setvariant(e.target.value) }}>
//             {piza.varients.map(varient => {
//               return <option value={varient}>{varient}</option>
//             })}
//           </select>
//         </div>
//         <div className='w-100 m-1'>
//           <p> Quantity</p>
//           <select className='form-control' value={quantity} onChange={(e) => { setquantity(e.target.value) }}>
//             {[...Array(10).keys()].map((x, i) => (
//               <option value={i + 1} key={i}>
//                 {i + 1}
//               </option>
//             ))}
//           </select>

//         </div>
//       </div>
//       <div className='flex-container'>
//         <div className='m-1 w-100'>
//           <h1 className='mt-1'>price: {piza.prices[0][variant] * quantity}Rs/-</h1>
//         </div>
//         <div className='m-1 w-100'>
//           <button className='btn'>ADD TO CART</button>
//         </div>
//       </div>

//       <Modal show={show} onHide={handleClose}>
//         <Modal.Header closeButton>
//           <Modal.Title>{piza.name}</Modal.Title>
//         </Modal.Header>

//         <Modal.Body>
//          <img src={piza.image}  className="img-fluid"style={{height:'300px !important'}}/>
//          <p>{piza.description}</p>
//         </Modal.Body>

//         <Modal.Footer>
//           <button className='btn' onClick={handleClose}>close</button>
//         </Modal.Footer>
//       </Modal>

//     </div>
//   )
// }

// export default Piechart

// import React from 'react'
// import {Button} from 'react-bootstrap'
// import './App.css';


// function Piechart() {
//     // const  calculateEMI = () =>{
//     //     let emi = loanAmount  * interest * (Math.pow (1 + interest, LoanTenure)/(Math.pow (1 + interest, LoanTenure) -1));
//     // }
//   return (

//     <>
//          <div class="loan-calculator">
//       <div class="top">
//         <h2>Loan Calculator</h2>

//         <form action="#">
//           <div class="group">
//             <div class="title">Amount</div>
//             <input type="text" value="30000" class="loan-amount" />
//           </div>

//           <div class="group">
//             <div class="title">Interest Rate</div>
//             <input type="text" value="8.5" class="interest-rate" />
//           </div>

//           <div class="group">
//             <div class="title">Tenure (in months)</div>
//             <input type="text" value="240" class="loan-tenure" />
//           </div>
//         </form>
//       </div>

//       <div class="result">
//         <div class="left">
//           <div class="loan-emi">
//             <h3>Loan EMI</h3>
//             <div class="value">123</div>
//           </div>

//           <div class="total-interest">
//             <h3>Total Interest Payable</h3>
//             <div class="value">1234</div>
//           </div>

//           <div class="total-amount">
//             <h3>Total Amount</h3>
//             <div class="value">12345</div>
//           </div>

//           <button class="calculate-btn">Calculate</button>
//         </div>

//         <div class="right">
//           <canvas id="myChart" width="400" height="400"></canvas>
//         </div>
//       </div>
//     </div>


  
//   </>


//   )
// }

// export default Piechart



import React, { useState } from 'react';
import './App.css';
function Piechart() {
  

  const [userValues, setUserValues] = useState({
    amount: '',
    interest: '',
    years: '',
  });



  const [results, setResults] = useState({
    monthlyPayment: '',
    totalPayment: '',
    totalInterest: '',
    isResult: false,
  });


  const [error, setError] = useState('');

  

  const handleInputChange = (event) =>
    setUserValues({ ...userValues, [event.target.name]: event.target.value });

 
    // const handleAmountInputChange = (event) =>
    //   setUserValues({ ...userValues, amount: event.target.value });

    // const handleInterestInputChange = (event) =>
    //   setUserValues({ ...userValues, interest: event.target.value });

    // const handleYearsInputChange = (event) =>
    //   setUserValues({ ...userValues, years: event.target.value });

 
  const isValid = () => {
    const { amount, interest, years } = userValues;
    let actualError = '';
    if (!amount || !interest || !years) {
      actualError = 'All the values are required';
    }


    // if (isNaN(amount) || isNaN(interest) || isNaN(years)) {
    //   actualError = 'All the values must be a valid number';
    // }


    if (Number(amount) <= 0 || Number(interest) <= 0 || Number(years) <= 0) {
      actualError = 'All the values must be a positive number';
    }
    if (actualError) {
      setError(actualError);
      return false;
    }
    return true;
  };
  const handleSubmitValues = (e) => {
    e.preventDefault();
    if (isValid()) {
      setError('');
      calculateResults(userValues);
    }
  };
  const calculateResults = ({ amount, interest, years }) => {
    const userAmount = Number(amount);
    const calculatedInterest = Number(interest) / 100 / 12;
    const calculatedPayments = Number(years) * 12;
    const x = Math.pow(1 + calculatedInterest, calculatedPayments);
    const monthly = (userAmount * x * calculatedInterest) / (x - 1);

    if (isFinite(monthly)) {
      const monthlyPaymentCalculated = monthly.toFixed(2);
      const totalPaymentCalculated = (monthly * calculatedPayments).toFixed(2);
      const totalInterestCalculated = (
        monthly * calculatedPayments -
        userAmount
      ).toFixed(2);


      setResults({
        monthlyPayment: monthlyPaymentCalculated,
        totalPayment: totalPaymentCalculated,
        totalInterest: totalInterestCalculated,
        isResult: true,
      });
    }
    return;
  };

  

  const clearFields = () => {
    setUserValues({
      amount: '',
      interest: '',
      years: '',
    });

    setResults({
      monthlyPayment: '',
      totalPayment: '',
      totalInterest: '',
      isResult: false,
    });
  };

  return (
    <>
    <div className='calculator' style={{marginTop:'50px'}}>
      <div className='form'>
        <h1>Loan Calculator</h1>
        
        <p className='error'>{error}</p>
        <form onSubmit={handleSubmitValues}>
         
          {!results.isResult ? (
           

            <div className='form-items'>
              <div>
                <label id='label'>Amount:</label>
                <input
                  type='text'
                  name='amount'
                  placeholder='Loan amount'
                  value={userValues.amount}
                  onChange={handleInputChange}
                />
              </div>
              <div>
                <label id='label'>Interest:</label>
                <input
                  type='text'
                  name='interest'
                  placeholder='Interest'
                  value={userValues.interest}
                  onChange={handleInputChange}
                />
              </div>
              <div>
                <label id='label'>Years:</label>
                <input
                  type='text'
                  name='years'
                  placeholder='Years to repay'
                  value={userValues.years}
                  onChange={handleInputChange}
                />
              </div>
              <input type='submit' className='button' />
            </div>
          ) : (
            

            <div className='form-items'>
              <h4>
                Loan amount: ${userValues.amount} <br /> Interest:{' '}
                {userValues.interest}% <br /> Years to repay: {userValues.years}
              </h4>
              <div>
                <label id='label'>Monthly Payment:</label>
                <input type='text' value={results.monthlyPayment} disabled />
              </div>
              <div>
                <label id='label'>Total Payment: </label>
                <input type='text' value={results.totalPayment} disabled />
              </div>
              <div>
                <label id='label'>Total Interest:</label>
                <input type='text' value={results.totalInterest} disabled />
              </div>
              

              <input
                className='button'
                value='Calculate again'
                type='button'
                onClick={clearFields}
              />
            </div>
          )}
        </form>
      </div>
    </div>

    <div  class='cointaner mx-5 text-center'>
      <h1 style={{color:'Green'}}>Steps to apply for personal loan</h1>
      <ol style={{color:'brown'}}>
        <li>Sign up using mobile number</li>
        <li>Enter basic information & check eligibility</li>
        <li>Verify your profile by uploading KYC documents</li>
        <li>Choose your loan amount and tenure</li>
        <li>Get loan disbursed directly into your bank account.</li>



      </ol>

    </div>
    </>
  );
}

export default Piechart;