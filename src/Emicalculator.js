import React from 'react'
import {Card,CardImg,CardText,CardTitle,Table} from 'react-bootstrap'

function Emicalculator(props) {
  return (
    <div class="grid-container col-lg3">
<div>
<Card style={{width:'100%',marginTop:'70px', textAlign:"center"}} >
        <CardImg  src="https://moneyview.in/images/dist/img/Google-Play-Badge-US.svg" style={{width:'250px'}} />
        <CardText style={{color:"blue"}}><h1>Personal Loan Eligibility</h1></CardText>
        <CardTitle style={{color:"Highlight"}}>Check Loan Eligibility Criteria in 2 Mins & Apply Now</CardTitle>
        </Card>

        <div><h1 style={{textAlign:'center'}}>EMI Schedule</h1></div>

<div style={{
  border: "1px solid black"
  
}}>
        <Table >
      <thead>
        <tr >
          <th style={{color:'seagreen'}} >Month</th>
          <th style={{ color:'seagreen'}}>Opening Balance</th>
          <th style={{ color:'seagreen'}}>Interest</th>
          <th style={{ color:'seagreen'}}>Principal</th>
          <th style={{ color:'seagreen'}}>Closing Balance</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Jan '01</td>
          <td>₹ 10,00,000</td>
          <td>₹ 13,333</td>
          <td>₹ 1,61,196</td>
          <td>₹ 8,38,803</td>       
        </tr>
        <tr>
          <td>Feb '01</td>
          <td>₹ 8,38,803</td>
          <td>₹ 11,184</td>
          <td>₹ 1,63,345</td>
          <td>₹ 6,75,457</td>
        </tr>
        <tr>
          <td>Mar '01</td>
          <td >	₹ 6,75,457</td>
          <td >	₹ 9,006</td>
          <td >₹ 1,65,523</td>
          <td >₹ 5,09,933</td>
         
        </tr>
        <tr>
          <td>Apr '01</td>
          <td >	₹ 5,09,933</td>
          <td>₹ 6,799</td>
          <td>	₹ 1,67,730</td>
          <td>₹ 3,42,202</td>
          
        </tr>
        <tr>
          <td>May '01	</td>
          <td>₹ 3,42,202</td>
          <td>₹ 4,563</td>
          <td>₹ 1,69,967</td>
          <td>₹ 1,72,235</td>
        </tr>
        <tr>
          <td>June '01</td>
          <td>₹ 1,72,235</td>
          <td>	₹ 2,296</td>
          <td>	₹ 1,72,233</td>
          <td>₹0.00</td>
        </tr>
      </tbody>
    </Table>
    </div>
    <div >
      <h1 style={{textAlign:'center'}}>Steps to apply for personal loan</h1>
      <ul>
        <li>Sign up using mobile number</li>
        <li>Enter basic information & check eligibility</li>
        <li>
        Verify your profile by uploading KYC documents
        </li>
        <li>Choose your loan amount and tenure</li>
        <li>Get loan disbursed directly into your bank account. </li>
      </ul>
    </div>
    </div>
    </div>
  )
}

export default Emicalculator