import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';


import { Button, Form, Dropdown, NavItem, Navbar, Container } from 'react-bootstrap'
import { BrowserRouter, Routes, Route, Link ,} from 'react-router-dom';
import Contactus from './Contactus';
import Login from './Login';
import Loans from './Loans';
import Moneyview from './Moneyview';
import Insertrate from './Insertrate';
import React, { useCallback } from 'react';
import Credit from './Credit';
import Register from './Register';
import Emicalculator from './Emicalculator';
import Eligibility from './Eligibility';
import Applynow from './Applynow';
import Piechart from './Piechart';
import Pie from './Pie';


function App() {
  return (
    <>
      <div >
        <Navbar expand="lg" className="bg-body-tertiary fixed-top" >
          <Container style={{backgroundColor:'palegoldenrod'}}>
            <Navbar.Brand href="/"><img src="https://dugtmg0pklp2w.cloudfront.net/mv_new_pwa_logo.svg " width={'250px'} /></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto gap-5">
                {/* <NavDropdown title="Loans" href="/InterestRate" id="basic-nav-dropdown">
                  <NavDropdown.Item href="InterestRate">Interest Rate</NavDropdown.Item>
                  <NavDropdown.Item href="Eligibilitycriteria">Eligibility criteria </NavDropdown.Item>
                  <NavDropdown.Item href="DocuemntRequired">Docuemnt Required</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title=" EMI Calculator" id="basic-nav-dropdown">
                  <NavDropdown.Item href="PersonalLoan">Personal Loan</NavDropdown.Item>
                  <NavDropdown.Item href="EducationLoan">Education Loan </NavDropdown.Item>
                  <NavDropdown.Item href="BussinessLoan">Bussiness Loan</NavDropdown.Item>
                </NavDropdown> */}

                 <Nav.Link href="Loans">Loans</Nav.Link> 
                <Nav.Link href="CreditTracker">Credit Tracker</Nav.Link>
                <Nav.Link href="ContactUs">ContactUs</Nav.Link> 
                <div className='text-end'>
          {/* <Form className="d-flex ">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
          </Form> */}
         </div>
         <Login /> 
                
              </Nav>
            </Navbar.Collapse>
          </Container>
         
        </Navbar>

         <BrowserRouter>
          <Routes>
            <Route path="/contactus" element={<Contactus />} />
            <Route path='/Insertrate' element={< Insertrate />} />
            <Route path='/' element={< Moneyview />} />
            <Route path='/CreditTracker' element={< Credit />} />
            <Route path='/InsertRate' element={<Insertrate />} />
            <Route path='/Login'element={<Login />} /> 
            <Route path='/Loans' element={< Loans/>} />
            <Route path='/Emicalculator' element={< Emicalculator/>} />
            <Route path='/Register'element={< Register/>} />
            <Route path='/Eligibility'element={< Eligibility/>} />
            <Route path='/Applynow'element={< Applynow/>} />
            <Route path='/Piechart'element={< Piechart/>}/>
            <Route path='/pie'element={< Pie/>} />      
          </Routes>
        </BrowserRouter> 
       
      </div>
     

    </>
  );
}

export default App;

