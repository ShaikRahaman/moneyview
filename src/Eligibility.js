import React from 'react'
import {Card,CardImg} from 'react-bootstrap'
import { Table } from 'reactstrap'

function Eligibility() {
  return (
   <>

<div >
<div class="row col-lg gap-5 mx-5">
<Card style={{width:'100%',marginTop:'70px', textAlign:"center"}} >
        <CardImg  src="https://d31bgfoj87qaaj.cloudfront.net/moneyview-open-graph-image-final.jpg"  style={{width:'950px'}} />
        </Card>
</div>

<div class='cointaner mx-5 text-center'>
  <div >
    <Card style={{marginTop:"50px"}} >
      <ul>
    <h1>How is Personal Loan Eligibility Calculated?</h1>
    Personal loan eligibility is calculated based on the borrower’s credit score, income, and age among other factors. 
    <br></br>In order to get an instant personal loan from moneyview, the following personal loan eligibility criteria will have to be fulfilled.
    <br/>
    <br/> 

    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="green" class="bi bi-check-circle" viewBox="0 0 16 16"> 
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"/>
</svg> 
Type of Employment
  You must be Salaried or Self Employed (own business).
  <br/>
   <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="green" class="bi bi-check-circle" viewBox="0 0 16 16"> 
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"/>
</svg> 
Monthly Income
Your monthly in-hand income should be Rs 13,500/- or more. 
<br/>


<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="green" class="bi bi-check-circle" viewBox="0 0 16 16"> 
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"/>
</svg> 
Income in Bank
Your salary must be directly credited to your bank account. 
<br/>


<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="green" class="bi bi-check-circle" viewBox="0 0 16 16"> 
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"/>
</svg> 
Credit Score
You must either have a minimum CIBIL score of 650 or a minimum Experian score of 650.
<br/>
 Low Credit Score? You may still get a loan based on our own credit-assessment model. 
<br/>


<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="green" class="bi bi-check-circle" viewBox="0 0 16 16"> 
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"/>
</svg> 
Age
Applicants must be between the ages of 21 years to 57 years. 

</ul>
</Card>
</div>
</div>
</div>



<div class= 'cointaner mx-5 text-center'>
    <br/>
<h4 style={{color:'green'}}>Features of moneyview Personal Loans</h4>
Moneyview personal loans come with the following unique features.
<Table  style={{  border: "1px solid black",
  }}>
      <tbody>
        <tr>
        <td>Loan Amount</td>
          <td>Rs. 5,000 to Rs. 5,00,000</td>
        </tr>
        <tr>
        <td>Interest Rate(per month)</td>
          <td>	
          1.33% and above</td>
        </tr>
        <tr>
        <td>Tenure</td>
          <td>	
3-60 months</td>
          
        </tr>
        <tr>
        <td>Application Type</td>
          <td>Completely Digital</td>
          
        </tr>

        <tr>
        <td>Processing Fee</td>
          <td>	
2% of the loan amount</td>
          </tr>
          <tr>
        <td>Foreclosure</td>
          <td>
            <li>No part payments are allowed
            </li>
            <li>For foreclosure of your loan, you should have paid a minimum of 6 EMIs</li>
          </td>
          </tr>
      </tbody>
      </Table>
      </div>

     <div>


      

     </div>




</>
   
  )
}

export default Eligibility