
import Carousel from 'react-bootstrap/Carousel';
import { Card,Button, CardImg, CardBody, CardText ,Table, CardTitle } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Emicalculator from './Emicalculator';
import axios from 'axios';
import {useEffect,useState} from 'react'


function Moneyview() {
  const naviagte=useNavigate()

   const handleClick = ()=>{
    naviagte('/Emicalculator')
   }
   const handleSubmit = ()=>{
    naviagte('/ApplyNow')
   }
   const handleInput = ()=>{
    naviagte('/Eligibility')
   } 
   const handleShow = ()=>{
    naviagte('/Piechart')

   }
   const handleinput = ()=>{
    naviagte('/Pie')
   }


   const [data,setData]=useState([])
useEffect(()=>{
  const fecthData=async()=>{
  try{
    let response=await axios.get("http://localhost:3001/fetch")
    console.log(response.data)
    setData(response.data)
  }catch (err){
    throw err
  }
}
fecthData()
},[])
   
  return (
    <>
<div class='cointaner mx-5 text-center' >
    <Carousel data-bs-theme="dark" style={{marginTop:"70px",backgroundColor:'skyblue'}}>
      <Carousel.Item>
        <img
          className="d-block w-25"
          src="https://moneyview.in/images/header_banner_image_desktop_v2.webp"
          alt="First slide"
        />
        <Carousel.Caption style={{marginTop:"50px"}}>
          <img src="https://tse3.mm.bing.net/th?id=OIP.JcD-VXzG80No495SUhrKeAHaFj&pid=Api&P=0&h=180" />
          <h1>Get started
instantly</h1>
          <p >Moneyview.inGet Instant Personal Loan upto Rs. 10 Lakhs | Moneyview
moneyview instant personal loans come with unique benefits as given below -. Get any amount from Rs. 5,000 to Rs. 10,00,000. Enjoy flexible repayment options that can extend up to 5 years. .</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-25 "
          src="https://tse4.mm.bing.net/th?id=OIP.g3ArNLkmYhdX7RYLa89vGQHaEK&pid=Api&P=0&h=180"
          alt="Second slide"
        />
        <Carousel.Caption>
        
          {/* <h5>Moneyview offer a Top up loan</h5> */}
          <Card.Text textAlign="center">
          <p>Moneyview offers top up loans to esteemed loans who have shown a stellar credit history and loan repayment.
            <br/> While the interest rate and documentation requirements are lower, one must have availed a Money View personal loan <br/>previously and repaid this on time in order to be eligible for a Money View top up loan. </p>
            </Card.Text>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-25"
          src="https://i.ytimg.com/vi/GJphBx-o8YY/maxresdefault.jpg"
          alt="Third slide"
        />
        <Carousel.Caption>
          <h5>Money View Charges </h5>
          <p>
          Apart from personal loans, the app offers you a smart money manager to help you in tracking expenses and budgeting. You can plan and make monthly budgets on the Money View app. But, on the other hand, Money View charges a high processing fee that varies between 2.5% to 4% of the loan amount.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>

    <div class="container mx-5 " >
<div class='row gap-5 mx-5' >
   {data.filter((e)=>(e.cardImg,e.cardText,e.cardTittle)).map((e)=>(
    <div className="col-lg-4 col-md-4 col-sm-12 card border-0 mt-3" key={e.cardImg}>
          <img src={e.cardImg} alt="" className='w-100 rounded-3'/>
          <div className='card-body text-center'>
            <p className='text-dark fs-5'>{e.cardText}</p>
            <p className='text-dark '>{e.cardTittle}</p>
          </div>
    </div>
  ))}
</div>
</div>

  

<div class= "cointaner mx-5 text-center">
<div class='row col-lg gap-5 mx-5' >
    <Card  className="col-lg-4 col-md-4 col-sm-12" style={{ width:'18rem', marginTop:"50px",backgroundColor:'lightgreen'}}>
      <Card.Img variant="top" src="https://dugtmg0pklp2w.cloudfront.net/ic-credit-info-card1.svg" onClick={handleClick}  />
      <Card.Body>
        <Card.Title>EMICalculator & Personal Loans</Card.Title>
        <Card.Text style={{color:'darkgreen'}}>
        Unique credit model
        <br/>
        Quick Disbursement
        <br/>
        Hassle-free Application Process
        <br/>
        Minimal Documentation
        </Card.Text>
      </Card.Body>
    </Card>

    <Card className='col-lg-4 col-md-4 col-sm-12' style={{ width: '18rem', marginTop:"50px",backgroundColor:'lightgreen' }}>
      <Card.Img variant="top" src="https://dugtmg0pklp2w.cloudfront.net/ic-credit-info-card2.svg" onClick={handleShow}/>
      <Card.Body>
        <Card.Title>EMi Calculator Here!</Card.Title>
        <Card.Text style={{color:'green'}}>
        repayment history
        <br/>
        credit utilization ratio,<br/>
         credit enquiries 
         <br/>
         credit mix
        </Card.Text>
      </Card.Body>
    </Card>
    <Card className='col-lg-4 col-md-4 col-sm-12' style={{ width: '18rem', marginTop:"50px",backgroundColor:'lightgreen'}}>
      <Card.Img variant="top" src="https://dugtmg0pklp2w.cloudfront.net/ic-credit-info-card3.svg" onClick={handleinput}/>
      <Card.Body style={{color:'green'}}>
        <Card.Title>Our Partner</Card.Title>
        <Card.Text>
        Credit score ranges between 300-900
        <br/>
        Get your credit report free, with monthly updates.
        <br/>
        Both secured and unsecured loans

        </Card.Text>
      </Card.Body>
    </Card>
    <Card style={{ width: '18rem', marginTop:"50px",backgroundColor:'lightgreen'}}>
      <Card.Img variant="top" src="https://dugtmg0pklp2w.cloudfront.net/ic-credit-info-card4.svg"  onClick={handleInput}/>
      <Card.Body style={{color:'green'}}>
        <Card.Title>Pre Approved Offers</Card.Title>
        <Card.Text>
        Instant loan facility for selected customers
        <br/>
        Personal Loan, 
        <br/>
        Business Loan and Insurance <br/>
         And more with Tata Capital.
        </Card.Text>
      </Card.Body>
    </Card>
    <Card style={{ width: '18rem',marginTop:"50px",backgroundColor:'lightgreen'}}>
      <Card.Img variant="top" src="https://tse1.mm.bing.net/th?id=OIP.5ZsLdvB-5vOwnrtsaDCVQgHaE7&pid=Api&P=0&h=180" onClick={handleSubmit} />
      <Card.Body style={{color:'green'}}>
        <Card.Title><u>Apply NOW </u> For Loan</Card.Title>
        <Card.Text>
        Instant loan facility for selected customers
        <br/>
        Personal Loan, 
        <br/>
        Business Loan and Insurance <br/>
         And more with Tata Capital.
        </Card.Text>
      </Card.Body>
    </Card>
    </div>
    </div>
    
    <div  class= "cointaner mx-5 text-center mt-3" >
      <Card >
        <p style={{color:'blue'}} > Customer Support
        <br/>

        <u>care@moneyview.in</u>
        <br/>
080 6939 0476</p>

<Table striped bordered hover style={{backgroundColor:'lightgreen'}}>
  <thead>
    <tr>
      <td>
      The Company
      </td>
      <td>Loans App</td>
      <td>Our Products</td>
    </tr>
  </thead>

  <tbody>
        <tr>
          <td>About</td>
          <td>Home</td>
          <td>Personal Loan</td>
        </tr>
        <tr>
          <td>Founding Member: DLAI</td>
          <td>Lending Partners</td>
          <td>Credit Tracker</td>
        </tr>
        <tr>
          <td>Security</td>
          <td>Business Partners
</td>
          <td>savings Account
(Coming Soon)</td>
        </tr>
        </tbody>
</Table>

{/* <Card >
<Card.Img  src="https://moneyview.in/images/linkedin-icon.webp" />
<br/>
<CardImg src="https://moneyview.in/images/instagram-icon.webp" />

</Card> */}
      </Card>
      
    </div>
<div >
</div>

</>
  );
}



export default Moneyview;